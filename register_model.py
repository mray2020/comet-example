import argparse
from comet_ml import API


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("--workspace", type=str)
    parser.add_argument("--project_name", type=str)
    parser.add_argument("--experiment_id", type=str)
    parser.add_argument("--model_name", type=str)
    parser.add_argument("--model_version", type=str)

    return parser.parse_args()


def register_model(workspace, project_name, experiment_id, model_name, model_version):
    api = API()
    experiment = api.get("%s/%s/%s" % (workspace, project_name, experiment_id))
    experiment.register_model(model_name=model_name, version=model_version)


def main():
    args = get_args()

    workspace = args.workspace
    project_name = args.project_name
    experiment_id = args.experiment_id

    model_name = args.model_name
    model_version = args.model_version

    register_model(workspace, project_name, experiment_id, model_name, model_version)


if __name__ == "__main__":
    main()
